<?php

namespace Laradex\Http\Controllers;

use Laradex\Trainer;
use Illuminate\Http\Request;

class TrainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trainers = Trainer::all();
        //consulta todos los entrenadores y los regresa
     return view('trainers.index', compact('trainers'));
     //compact genera un array con la información que le 
     //asignemos
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('trainers.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('avatar')){
            $file = $request->file('avatar');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/images', $name);

           
        }          
            //obtenemos el nombre original 
            //y validamos que el nombre nunca 
            //se repita getClientOriginalName
            
             $trainer = new Trainer();
             $trainer->name = $request->input('name');
             $trainer->avatar = $name;
             $trainer->save();
                   return 'El avatar ha sido guardado exitosamente.';

        //ESTE SOLO RETORNA EL NOMBRE
        //return $request->input('name');
        //este retorna todo, nombre y el token generado:
     //return $request->all();

        }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
